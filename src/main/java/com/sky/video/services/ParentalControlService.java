package com.sky.video.services;

import com.sky.video.enums.ParentalControlLevel;
import com.sky.video.exceptions.TechnicalFailureException;
import com.sky.video.exceptions.TitleNotFoundException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.sky.video.enums.ParentalControlLevel.isValid;

public class ParentalControlService {

    private static Logger logger = LoggerFactory.getLogger(ParentalControlService.class);

    private MovieService movieService;

    public ParentalControlService(MovieService movieService) {
        this.movieService = movieService;
    }

    public boolean hasAccess(String customerPreferenceLevel, String movieId) {

        if (StringUtils.isBlank(customerPreferenceLevel)
                || !isValid(customerPreferenceLevel)
                || StringUtils.isBlank(movieId)) {
            return false;
        }

        boolean hasAccess = false;

        try {
            hasAccess = isAllowedToWatch(customerPreferenceLevel, movieService.getParentalControlLevel(movieId));
        } catch (TitleNotFoundException e) {
            logger.error("The movie titled [{}] has not been found", movieId);
        } catch (TechnicalFailureException e) {
            logger.error("System error : The movie titled [{}] cannot be watched", movieId);
        }

        return hasAccess;
    }

    private boolean isAllowedToWatch(String customerPreferenceLevel, String parentalControlLevel) {
        return orderOf(parentalControlLevel) <= orderOf(customerPreferenceLevel);
    }

    private int orderOf(String level) {
        return ParentalControlLevel.get(level).getOrder();
    }
}
