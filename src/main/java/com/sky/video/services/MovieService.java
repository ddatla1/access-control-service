package com.sky.video.services;

import com.sky.video.exceptions.TechnicalFailureException;
import com.sky.video.exceptions.TitleNotFoundException;

public interface MovieService {
    String getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException;
}

