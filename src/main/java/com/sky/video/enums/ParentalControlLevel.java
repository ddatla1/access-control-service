package com.sky.video.enums;

import java.util.HashMap;
import java.util.Map;

public enum ParentalControlLevel {
    LEVEL_U(1, "U"),
    LEVEL_PG(2, "PG"),
    LEVEL_12(3, "12"),
    LEVEL_15(4, "15"),
    LEVEL_18(5, "18");

    private final int order;
    private final String level;

    private static final Map<String, ParentalControlLevel> lookup = new HashMap<>();

    static {
        for (ParentalControlLevel parentalControlLevel : ParentalControlLevel.values()) {
            lookup.put(parentalControlLevel.getLevel(), parentalControlLevel);
        }
    }

    private ParentalControlLevel(int order, String level) {
        this.order = order;
        this.level = level;
    }

    public int getOrder() {
        return order;
    }

    public String getLevel() {
        return level;
    }

    public static ParentalControlLevel get(String level) {
        return lookup.get(level);
    }

    public static boolean isValid(String level) {
        return lookup.containsKey(level);
    }
}
