package com.sky.video.services;

import com.sky.video.exceptions.TechnicalFailureException;
import com.sky.video.exceptions.TitleNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceTest {

    private ParentalControlService parentalControlService;

    @Mock
    private MovieService movieService;

    @Before
    public void setup() {
        parentalControlService = new ParentalControlService(movieService);
    }

    @Test
    public void hasAccess_WithParentalControlLevelMatchingCustomerPreference_ShouldReturnTrue()
            throws TechnicalFailureException, TitleNotFoundException {

        // Arrange
        when(movieService.getParentalControlLevel(anyString())).thenReturn("PG");

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("PG", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(true));
    }

    @Test
    public void hasAccess_WithParentalControlLevelLessThanCustomerPreference_ShouldReturnTrue()
            throws TechnicalFailureException, TitleNotFoundException {

        // Arrange
        when(movieService.getParentalControlLevel(anyString())).thenReturn("U");

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("PG", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(true));
    }


    @Test
    public void hasAccess_WithParentalControlLevelGreaterThanCustomerPreference_ShouldReturnFalse()
            throws TechnicalFailureException, TitleNotFoundException {

        // Arrange
        when(movieService.getParentalControlLevel(anyString())).thenReturn("18");

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("PG", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WithTitleNotFoundException_ShouldReturnFalse()
            throws TechnicalFailureException, TitleNotFoundException {

        // Arrange
        when(movieService.getParentalControlLevel(anyString())).thenThrow(new TitleNotFoundException());

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("PG", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WithTechnicalFailureException_ShouldReturnFalse()
            throws TechnicalFailureException, TitleNotFoundException {

        // Arrange
        when(movieService.getParentalControlLevel(anyString())).thenThrow(new TechnicalFailureException());

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("PG", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WithInvalidCustomerPreferenceParameter_ShouldReturnFalse() {

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("INVALID", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WithEmptyCustomerPreferenceParameter_ShouldReturnFalse() {

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess(" ", "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WithNullCustomerPreferenceParameter_ShouldReturnFalse() {

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess(null, "aMovieId");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WitEmptyMovieIdParameter_ShouldReturnFalse() {

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("U", " ");

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }

    @Test
    public void hasAccess_WithNullMovieIdParameter_ShouldReturnFalse() {

        // Act
        boolean hasAccessToWatch = parentalControlService.hasAccess("U", null);

        // Assert
        assertThat(hasAccessToWatch, equalTo(false));
    }
}
