# Parental Control Service
Prevents access to movies based on parental control level

## Requirements

- `Java 1.8`
- `Maven`

## Compile and Run Tests

This project has been built using maven

- `mvn compile` to compile source code
- `mvn test` to run unit tests